package ru.dfr.attestation01_oop;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findById(int id) {

        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                if (Integer.parseInt(parts[0]) != id) {
                    line = bufferedReader.readLine();
                } else {
                    return new User(Integer.parseInt(parts[0]),
                            parts[1], Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3]));
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {}
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {}
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
        List<User> users = this.findAll();
        User[] arrayUsers = users.toArray(new User[users.size()]);
        for (int i = 0; i < arrayUsers.length; i++) {
            if (arrayUsers[i].getId() == user.getId()) {
                arrayUsers[i] = user;
            }
        }
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, false);
            bufferedWriter = new BufferedWriter(writer);

            for (int i = 0; i < arrayUsers.length; i++) {
                bufferedWriter.write(arrayUsers[i].getId() + "|" + arrayUsers[i].getName() + "|" + arrayUsers[i].getAge() + "|" + arrayUsers[i].getIsWorker());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush(); // сброс буфера
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignored) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignored) {}
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {

        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                User user = new User(Integer.parseInt(parts[0]),
                        parts[1], Integer.parseInt(parts[2]),
                        Boolean.parseBoolean(parts[3]));

                if (user.getAge() == age) {
                    users.add(user);
                }

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {}
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {}
            }
        }
        return users;
    }

    @Override
    public List<User> findByIsWorker() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                User user = new User(Integer.parseInt(parts[0]),
                        parts[1], Integer.parseInt(parts[2]),
                        Boolean.parseBoolean(parts[3]));

                if (user.getIsWorker()) {
                    users.add(user);
                }

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {}
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {}
            }
        }
        return users;
    }

    @Override
    public void save(User user) {

        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.getIsWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush(); // сброс буфера
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignored) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignored) {}
            }
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                User user = new User(Integer.parseInt(parts[0]),
                        parts[1], Integer.parseInt(parts[2]),
                        Boolean.parseBoolean(parts[3]));
                users.add(user);

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try{
                    reader.close();
                } catch (IOException ignored) {}

            }
            if (bufferedReader !=  null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {}
            }
        }
        return users;
    }
}
