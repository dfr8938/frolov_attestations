package ru.dfr.attestation01_oop;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();
    void save(User user);
    List<User> findByAge(int age);
    List<User> findByIsWorker();
    User findById(int id);
    void update(User user);
}

