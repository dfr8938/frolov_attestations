package ru.dfr.attestation01_oop;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        String fileName = "users.txt";

        UsersRepository userRepository = new UsersRepositoryFileImpl(fileName);
        List<User> users = userRepository.findAll();

        printTitle("findById");
        User user = userRepository.findById(2);
        printUser(user);

        user.setName("Марсель");
        user.setAge(27);

        printTitle("update");
        userRepository.update(user);
        printListUsers(users);

        User user1 = userRepository.findById(3);
        printUser(user1);

        user1.setName("Маша");
        user1.setAge(18);

        printTitle("update");
        userRepository.update(user1);
        printListUsers(users);
    }

    public static void printUser(User user) {
        System.out.printf("ID: %d, Name: %s, age: %d, isWorker: %b\n",
                user.getId(), user.getName(), user.getAge(), user.getIsWorker());
    }

    public static void printListUsers(List<User> users) {
        for (User user : users) {
            System.out.printf("ID: %d, Name: %s, age: %d, isWorker: %b\n",
                    user.getId(), user.getName(), user.getAge(), user.getIsWorker());
        }
    }

    // проверка на повторную запись
    public static boolean equalsUsers(List<User> users, User newUser) {

        for (User user : users) {
            if (user.getName().equals(newUser.getName())) {
                if (user.getAge() == newUser.getAge()) {
                    if (user.getIsWorker() == newUser.getIsWorker()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // препятствует записи повторного юзера
    public static void notWriteRepeatUser(List<User> users, User newUser, UsersRepository userRepository) {
        if (!equalsUsers(users, newUser)) {
            userRepository.save(newUser);
        }
    }

    public static void printTitle(String str) {
        System.out.println("-------------------------");
        System.out.printf("Вывод метода %s():\n", str);
        System.out.println("-------------------------");
    }
}

